﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Students : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<Student> students = StudentBusiness.GetStudents();
            foreach(Student s in students)
            {
                if (s.budget)
                    ListBox1.Items.Add(s.id.ToString() + " " + s.fistname + " " + s.lastname);
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.fistname = TextBox1.Text;
            s.lastname = TextBox2.Text;
            s.budget = CheckBox1.Checked;
            StudentBusiness.AddStudent(s);
        }
    }
}