﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
namespace BusinessLayer
{
    public class StudentBusiness
    {
  static        private StudentRepository sr = new StudentRepository();

        public static List<Student> GetStudents()
        {
            return sr.GetStudents();
        }

        public static bool AddStudent(Student s)
        {
            if(sr.AddStudent(s) > 0)
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
